package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors

        int n = 1; //n for while loop
        

        while(n==1){ //first loop that will run after round with correct input
            int t = 1;
            System.out.printf("Let's play round %d\n", roundCounter); //Printer ut tekst og hvilken runde
            
            while (t==1){
                String player = readInput("Your choice (Rock/Paper/Scissors)?");//Asks the player for input
                Random rand = new Random();
                int max = 3;
                int randomint = rand.nextInt(max);
                String comp=rpsChoices.get(randomint);

                //Tester for hvem som vinner, og gir poeng til vinner
                if (rpsChoices.contains(player)==false){
                    System.out.printf("I do not understand %s. Could you please try again?\n", player);
                    continue;
                }
                else if (player.equals(comp)){
                    System.out.printf("Human chose %s, computer chose %s. Its a tie!\n", player,comp);
                }
                else if (player.equals("paper") && comp.equals("rock")){
                    System.out.printf("Human chose %s, computer chose %s. User wins!\n", player,comp);
                    humanScore += 1;
                }
                else if (player.equals("rock") && comp.equals("scissors")){
                    System.out.printf("Human chose %s, computer chose %s. User wins!\n", player,comp);
                    humanScore += 1;
                }
                else if (player.equals("scissors") && comp.equals("paper")){
                    System.out.printf("Human chose %s, computer chose %s. User wins!\n", player,comp);
                    humanScore += 1;
                }
                else {
                    System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", player,comp);
                    computerScore += 1;
                }
                
                //Printer ut score
                System.out.printf("Score: human %s, computer %s\n", humanScore,computerScore);

                //While løkke som spør om spiller vil fortsette
                while(t==1){
                    String answer=readInput("Do you wish to continue playing? (y/n)?");
                    if (answer.equals("y")){
                        roundCounter+=1;
                        t=0;
                    }
                    else if(answer.equals("n")){
                        System.out.println("Bye bye :)");
                        t=0;
                        n=0;
                    }
                    else{
                        System.out.println("Vennligst skriv y eller n");
                    }
                }

            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
